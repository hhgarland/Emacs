(require 'package)
(setq package-enable-at-startup nil)
(setq package-archives '(("gnu"   . "http://elpa.gnu.org/packages/")
			 ("melpa" . "https://melpa.org/packages/")))
(package-initialize)

;; use-package setup
;; bootstrap `use-package'
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(require 'use-package)
(setq use-package-always-ensure 't)

;; basic settings
(use-package emacs
  :config
  (setq ring-bell-function 'ignore
	frame-resize-pixelwise t
	default-directory "~/"
	inhibit-startup-screen t)
  (column-number-mode)
  (menu-bar-mode -1)
  (scroll-bar-mode -1)
  (set-frame-font "JetBrains Mono-15")
  (set-frame-parameter (selected-frame) 'alpha '(100 . 100))
  (show-paren-mode)
  (tool-bar-mode -1)
  (tooltip-mode -1))

;; keep emacs custom settings in separate file
(setq custom-file (expand-file-name "~/.config/emacs/custom.el"))
(load custom-file t)

;; disable autosave
(setq auto-save-file-name-transforms '(("." "~/.config/emacs/auto-save-list" t)))

;; company
(use-package company
  :ensure t
  :init
  (add-hook 'after-init-hook 'global-company-mode t))

;; counsel config
(use-package counsel
  :ensure t
  :bind (("M-x" . counsel-M-x)))

;; diff-hl
(use-package diff-hl
  :ensure t
  :init
  (global-diff-hl-mode))

;; eldoc
(use-package eldoc
  :ensure t
  :config
  (setq eldoc-idle-delay 0.4))

;;;; elfeed stuff
;; elfeed
(use-package elfeed
  :ensure t
  :config
  (setq elfeed-db-directory (expand-file-name "elfeed" user-emacs-directory)
	elfeed-show-entry-switch 'display-buffer)
  :bind
  ("C-x e" . elfeed))

;; elfeed-org
(use-package elfeed-org
  :ensure t
  :after (elfeed)
  :config
  (setq rmh-elfeed-org-files '("~/.config/emacs/elfeed.org"))
  (elfeed-org))

;; use monospace fonts for elfeed reader
(setq shr-use-fonts nil)

;;;; evil stuff
;; evil
(use-package evil
  :ensure t
  :bind (("<escape>" . keyboard-escape-quit))
  :init
  (setq evil-want-keybinding nil)
  (setq evil-undo-system 'undo-fu)
  :config
  (evil-mode 1))

;; vim bindings everywhere else
(use-package evil-collection
  :after evil
  :config
  (setq evil-want-integration t)
  (evil-collection-init))

;; evil-org
(use-package evil-org
  :ensure t
  :after org
  :hook (org-mode . (lambda () evil-org-mode))
  :config
  (require 'evil-org-agenda)
  (evil-org-agenda-set-keys))

;; undo-fu
(use-package undo-fu)

;; flycheck
(use-package flycheck
  :ensure t
  :init
  (global-flycheck-mode t))

;; gruvbox theme
(use-package gruvbox-theme
  :ensure t
  :config
  (load-theme 'gruvbox-dark-medium t))

;; ivy
(use-package ivy
  :ensure t
  :config
  (ivy-mode 1)
  (setq ivy-use-virtual-buffers t)
  (setq enable-recursive-minibuffers t))
  (setq ivy-re-builders-alist
  '((t . ivy--regex-ignore-order)))

;; lsp mode
(use-package lsp-mode
  :init
  ;; set prefix for lsp-command-keymap (few alternatives - "C-l", "C-c l")
  (setq lsp-keymap-prefix "C-c l")
  :hook (;; replace XXX-mode with concrete major-mode(e. g. python-mode)
     (python-mode . lsp)
     ;; if you want which-key integration
     (lsp-mode . lsp-enable-which-key-integration))
  :commands lsp)

;; lsp-ui
(use-package lsp-ui
  :commands lsp-ui-mode)

;; lsp-ivy
(use-package lsp-ivy
  :commands lsp-ivy-workspace-symbol)

;; lsp-treemacs
(use-package lsp-treemacs
  :commands lsp-treemacs-errors-list)

;; magit
(use-package magit
  :ensure t
  :bind (("C-x g" . magit-status)))

;; markdown mode
(use-package markdown-mode
  :ensure t
  :commands (markdown-mode gfm-mode)
  :mode (("README\\.md\\'" . gfm-mode)
     ("\\.md\\'" . markdown-mode)
     ("\\.markdown\\'" . markdown-mode))
  :init (setq markdown-command "multimarkdown"))

;; minions
(use-package minions
  :ensure t
  :config
  (minions-mode 1))

;; moody
(use-package moody
  :disabled
  :config
  (setq x-underline-at-descent-line t)
  (moody-replace-mode-line-buffer-identification)
  (moody-replace-vc-mode)
  (moody-replace-eldoc-minibuffer-message-function))

;; org-babel languages
(org-babel-do-load-languages
 'org-babel-load-languages
 '((ditaa . t))) ; this line activates ditaa

;; org-superstar-mode
(use-package org-superstar
  :ensure t
  :config
  (require 'org-superstar)
  (add-hook 'org-mode-hook (lambda () (org-superstar-mode 1))))

;; org-mode capture
(setq org-default-notes-file (concat org-directory "/notes.org"))
(define-key global-map "\C-cc" 'org-capture)

;; projectile
(use-package projectile
  :ensure t
  :config
  (define-key projectile-mode-map (kbd "C-x p") 'Projectile-command-map)
  (projectile-mode +1))

;; lsp-pyright
(use-package lsp-pyright
  :ensure t
  :hook (python-mode . (lambda ()
		      (require 'lsp-pyright)
		      (lsp))))  ; or lsp-deferred

;; rainbow delimiters
(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

;; rainbow-mode
(use-package rainbow-mode
  :ensure t)

;; smartparens
(use-package smartparens
  :ensure t
  :config
  (progn
(require 'smartparens-config)
(smartparens-global-mode 1)))

;; treemacs
(use-package treemacs
  :ensure t)

;; treemacs-evil
(use-package treemacs-evil
  :ensure t)

;; treemacs projectile
(use-package treemacs-projectile
  :ensure t)

;; which key
(use-package which-key
  :ensure t
  :init
  (setq which-key-separator " ")
  (setq which-key-prefix-prefix "+")
  :config
  (which-key-mode))

;; winum
(use-package winum
  :ensure t
  :config
  (global-set-key (kbd "M-0") 'treemacs-select-window)
  (global-set-key (kbd "M-1") 'winum-select-window-1)
  (global-set-key (kbd "M-2") 'winum-select-window-2)
  (global-set-key (kbd "M-3") 'winum-select-window-3)
  (global-set-key (kbd "M-4") 'winum-select-window-4)
  (global-set-key (kbd "M-5") 'winum-select-window-5)
  (global-set-key (kbd "M-6") 'winum-select-window-6)
  (global-set-key (kbd "M-7") 'winum-select-window-7)
  (global-set-key (kbd "M-8") 'winum-select-window-8)
  (winum-mode))

;; yaml-mode
(use-package yaml-mode
  :ensure t)
